import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResumeComponent } from './home-page/resume/resume.component';
import { AchiveComponent } from './project-page/achive/achive.component';

const routes: Routes = [
  {
    path: '', loadChildren: () =>
      import('./home-page/home-page.module').then(min => min.HomePageModule),component: ResumeComponent
  },
  {
    path: 'project', loadChildren: () =>
      import('./project-page/project-page.module').then(min => min.ProjectPageModule),component: AchiveComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
