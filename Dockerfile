# ใช้ Node.js เป็นภาพของเบส
# FROM node:latest as build

# กำหนดตัวแปรแวดล้อมสำหรับการสร้างโปรเจค Angular ในโหมดการสร้าง
# ARG configuration=production

# กําหนดโฟลเดอร์งานใน img ของ Node.js
# WORKDIR /app

# คัดลอกและวางไฟล์ package.json และ package-lock.json เพื่อติดตั้ง dependencies
# COPY package*.json ./

# ติดตั้ง dependencies ด้วย npm
# RUN npm install

# คัดลอกไฟล์และโฟลเดอร์ของโปรเจค Angular ไปยัง image ของ Node.js
# COPY . /app/

# สร้างโปรเจค Angular ด้วยคําสั่ง ng build
# RUN npm run build -- --configuration $configuration

# สร้าง image ของ Nginx เพื่อให้โปรเจค Angular ทํางาน
# FROM nginx:latest

# คัดลอกไฟล์ที่สร้างจาก image ของ Node.js ไปยัง image ของ Nginx
# COPY --from=build /app/dist/app /usr/share/nginx/html

# คอนฟิก Nginx เพื่อให้สามารถเปิดใช้งานโปรเจค Angular ได้บนพอร์ต 80
# EXPOSE 80

# เริ่ม Nginx เมื่อ image ของเราถูกเรียกใช้งาน
# CMD ["nginx", "-g", "daemon off;"]

FROM node:14.20

WORKDIR /app

COPY . .

RUN npm install

EXPOSE 4200

ENTRYPOINT [ "npm", "start" ]

