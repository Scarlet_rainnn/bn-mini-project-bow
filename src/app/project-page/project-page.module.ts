import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';

import { ProjectPageRoutingModule } from './project-page-routing.module';
import { AchiveComponent } from './achive/achive.component';

console.log("lazy load 2 worked!")
@NgModule({
  declarations: [
    AchiveComponent
  ],
  imports: [
    CommonModule,
    ProjectPageRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    FormsModule
  ],
})
export class ProjectPageModule { }
