import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

  getPokemonApi() {
    return this.http.get('https://pokeapi.co/api/v2/pokedex/1')
      .pipe(map((res: any) =>
        this.convertData(res['pokemon_entries'])
      ))
  }

  convertData(array: Array<any>) {
    let data = []
    for (let index = 0; index < array.length; index++) {
      let _getData = {
        "entry_number": array[index]['entry_number'],
        "name": array[index]['pokemon_species']['name'],
        "url": 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + array[index]['entry_number'] + '.png',
      }
      data.push(_getData)
    }
    return data
  }
}
