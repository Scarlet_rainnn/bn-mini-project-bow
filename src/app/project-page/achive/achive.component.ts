import { Component,OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  entry_number: number;
  name: string;
  url: string;
}

const matTableData: PeriodicElement[] = []

@Component({
  selector: 'app-achive',
  templateUrl: './achive.component.html',
  styleUrls: ['./achive.component.scss']
})
  
export class AchiveComponent implements OnInit {

  constructor(
    private router: Router,
    private dataSer: ServiceService
  ) {
    this.dataSrc = new MatTableDataSource<PeriodicElement>(matTableData);
  }

  num: any;
  output1: any = '';
  output2: any = '';
  showResults: boolean = false;

  loop1(num: any) {

    this.output1 = '';
    this.output2 = '';

    for (let index = 1; index <= num; index++) {
      console.log('*'.repeat(index))
      this.output1 += '*'.repeat(index) + '<br>';
    }
    for (let index = num - 1; index >= 1; index--) {
      console.log('*'.repeat(index))
      this.output1 += '*'.repeat(index) + '<br>';
    }

    for (let index = num; index >= 1; index--) {
      console.log('*'.repeat(index))
      this.output2 += '*'.repeat(index) + '<br>';
    }
    for (let index = 2; index <= num; index++) {
      console.log('*'.repeat(index))
      this.output2 += '*'.repeat(index) + '<br>';
    }

    this.showResults = true;
  }

  ngOnInit(): void {
    this.getPokemon()
  }
  ngAfterViewInit() {
    this.dataSrc.paginator = this.paginator1;
  }

  homeBt() {
    this.router.navigateByUrl('');
  };
  projectBt() {
    this.router.navigateByUrl('/project');
  };

  dataSrc = new MatTableDataSource<PeriodicElement>([]);
  displayColumns: any[] = ['entry_number', 'name', 'url']
  @ViewChild('paginator1', { static: true }) paginator1: MatPaginator | any;

  getPokemon() {
    this.dataSer.getPokemonApi().subscribe(data => {

      this.dataSrc.data = data
      console.log("Converted response ", this.dataSrc.data)
    })
  }
}
