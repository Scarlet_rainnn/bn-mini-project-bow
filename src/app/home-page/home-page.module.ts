import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { ResumeComponent } from './resume/resume.component';

console.log("lazy load 1 worked!")
@NgModule({
  declarations: [
    ResumeComponent
  ],
  imports: [
    CommonModule,
    HomePageRoutingModule
  ]
})
export class HomePageModule { }
